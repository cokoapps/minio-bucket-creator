# minio-bucket-creator

Minio client for bucket creation

## Instructions

Provide the following `ENV` variables:

```
export S3_URL (URL of the minio deployment)
export S3_ACCESS_KEY_ID_ADMIN (admin's username)
export S3_SECRET_ACCESS_KEY_ADMIN (admin's password)
export S3_ACCESS_KEY_ID_USER (username of the app that will use minio)
export S3_SECRET_ACCESS_KEY_USER (password of the app that will use minio)
export S3_BUCKET (name of the bucket)
```

Execute `docker-compose up`
